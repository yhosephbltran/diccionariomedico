package com.beltran.yhoseph.diccionariomedico;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.beltran.yhoseph.diccionariomedico.models.Area;
import com.beltran.yhoseph.diccionariomedico.models.Concept;
import com.dataaccess.yhoseph.ORM.models.DBEntity;

import java.util.List;

public class New_concept extends AppCompatActivity {
    private List<Area> areas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_concept);
        areas = DBEntity.getAll(Area.class);
        Spinner spinner = findViewById(R.id.area_id);
        ArrayAdapter<Area> adapter =
                new ArrayAdapter<Area>(getApplicationContext(), R.layout.spinner_item, areas);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(adapter);
        BottomNavigationView navigation = findViewById(R.id.create_menu_id);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, Home.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void save_concept() {
        Spinner mySpinner = findViewById(R.id.area_id);
        Area area = areas.get(mySpinner.getSelectedItemPosition());
        EditText name_edit =  findViewById(R.id.concept_name_id);
        EditText translate_edit = findViewById(R.id.concept_translate_id);
        EditText description_edit = findViewById(R.id.concept_description_id);
        Concept concept = new Concept();
        concept.set_concept_area_id(area.get_area_id());
        concept.set_concept_custom(true);
        concept.set_concept_like(true);
        concept.set_concept_name(name_edit.getText().toString());
        concept.set_concept_translate(translate_edit.getText().toString());
        concept.set_concept_definition(description_edit.getText().toString());
        concept.save();
        navigateUpTo(new Intent(this, Home.class));
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.save_button_id:
                    save_concept();
                    return true;
            }
            return false;
        }
    };
}
