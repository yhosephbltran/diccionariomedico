package com.beltran.yhoseph.diccionariomedico.models;

import com.dataaccess.yhoseph.ORM.annotations.OneToMany;
import com.dataaccess.yhoseph.ORM.annotations.Options;
import com.dataaccess.yhoseph.ORM.annotations.Table;
import com.dataaccess.yhoseph.ORM.models.DBEntity;

import java.util.List;

@Table(name = "AREA")
public class Area extends DBEntity {
    @Options(Primary = true, AutoIncrement = true)
    private int area_id;
    @Options(Trim = true)
    private String area_name;
    @OneToMany(many = Concept.class,oneId = "area_id", manyId = "concept_id_area")
    private List<Concept> concepts;

    public Area() {
    }

    public Area(String name) {
        this.area_name = name;
    }

    public String get_name() {
        return area_name;
    }

    public int get_area_id() {
        return area_id;
    }

    public void set_area_id(int ARE_ID) {
        this.area_id = ARE_ID;
    }

    public void set_name(String name) {
        this.area_name = name;
    }

    @Override
    public String toString() {
        return area_name;
    }

    public List<Concept> getConcepts() {
        return concepts;
    }
}
