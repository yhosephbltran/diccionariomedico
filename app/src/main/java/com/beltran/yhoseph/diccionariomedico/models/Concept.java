package com.beltran.yhoseph.diccionariomedico.models;

import com.dataaccess.yhoseph.ORM.annotations.ManyToOne;
import com.dataaccess.yhoseph.ORM.annotations.Options;
import com.dataaccess.yhoseph.ORM.annotations.Table;
import com.dataaccess.yhoseph.ORM.models.DBEntity;

@Table(name = "CONCEPT")
public class Concept extends DBEntity {

    @Options(Primary = true, AutoIncrement = true)
    private int concept_id;
    @Options(Trim = true)
    private String concept_name;
    @Options(Trim = true)
    private String concept_translate;
    private int concept_id_area;
    private String concept_definition;
    private boolean concept_like;
    private boolean concept_custom;

    @ManyToOne(manyId = "concept_id_area", oneId = "area_id")
    private Area area;

    public void set_concept_area_id(int concept_area_id) {
        this.concept_id_area = concept_area_id;
    }

    public void set_concept_definition(String concept_definition) {
        this.concept_definition = concept_definition;
    }

    public void set_concept_id(int concept_id) {
        this.concept_id = concept_id;
    }

    public void set_concept_name(String concept_name) {
        this.concept_name = concept_name;
    }

    public void set_concept_translate(String concept_translate) {
        this.concept_translate = concept_translate;
    }

    public int get_concept_area_id() {
        return concept_id_area;
    }

    public int get_concept_id() {
        return concept_id;
    }

    public String get_concept_definition() {
        return concept_definition;
    }

    public String get_concept_name() {
        return concept_name;
    }

    public String get_concept_translate() {
        return concept_translate;
    }

    public boolean isConcept_like() {
        return concept_like;
    }

    public void set_concept_like(boolean concept_like) {
        this.concept_like = concept_like;
    }

    public boolean isConcept_custom() {
        return concept_custom;
    }

    public void set_concept_custom(boolean concept_custom) {
        this.concept_custom = concept_custom;
    }

    public Area get_area() {
        return area;
    }

    @Override
    public String toString() {

        return concept_name;
    }

}
