package com.beltran.yhoseph.diccionariomedico;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.beltran.yhoseph.diccionariomedico.models.Area;
import com.beltran.yhoseph.diccionariomedico.models.Concept;

import java.util.List;

public class List_adapter extends ArrayAdapter<Concept> {
    private final Context context;
    private List<Concept> concepts;
    private List<Area> areas;
    private String search;
    public List_adapter(Context context, List<Concept> concepts, List<Area> areas) {
        super(context, R.layout.item_list, concepts);
        this.context = context;
        this.concepts = concepts;
        this.areas = areas;
    }

    public void setSearch(String search) {
        this.search = search;
    }
    public void deleteSearch(){
        this.search = null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_list, parent, false);
        TextView TitleItem = rowView.findViewById(R.id.TitleItem);
        if(this.search==null){
            TitleItem.setText(this.concepts.get(position).get_concept_name().toUpperCase());
        }else{
            String text = this.concepts.get(position).get_concept_name().toUpperCase();
            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
            SpannableStringBuilder ssBuilder = new SpannableStringBuilder(text);
            ssBuilder.setSpan(
                    boldSpan,
                    text.indexOf(this.search.toUpperCase()),
                    text.indexOf(this.search.toUpperCase()) + String.valueOf(this.search.toUpperCase()).length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            );
            TitleItem.setText(ssBuilder);
        }

        TextView AreaItem = rowView.findViewById(R.id.AreaItem);
        AreaItem.setText(getAreaName(this.concepts.get(position).get_concept_area_id()));
        return rowView;
    }

    private String getAreaName(int termino_area_id) {
        String res = "";
        for (int i = 0; i < areas.size(); i++) {
            if (areas.get(i).get_area_id() == termino_area_id) {
                res = areas.get(i).get_name();
                break;
            }
        }
        return res;
    }

    public void updateReceiptsList(List<Concept> newlist) {
        concepts.clear();
        concepts.addAll(newlist);
        this.notifyDataSetChanged();
    }

    public void addReceiptsList(List<Concept> newlist) {
        this.concepts.addAll(newlist);
        this.notifyDataSetChanged();
    }
}