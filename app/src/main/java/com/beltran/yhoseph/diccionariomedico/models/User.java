package com.beltran.yhoseph.diccionariomedico.models;

import com.dataaccess.yhoseph.ORM.annotations.Table;
import com.dataaccess.yhoseph.ORM.models.DBEntity;

@Table
public class User extends DBEntity {
    private String user_username;
    private String user_password;
    public User(){}

    @Override
    public String toString() {
        return "USER: " + this.user_username + "  PASSWORD: " + this.user_password;
    }
}
