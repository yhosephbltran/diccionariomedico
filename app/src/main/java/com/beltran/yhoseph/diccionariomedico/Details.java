package com.beltran.yhoseph.diccionariomedico;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;

import com.beltran.yhoseph.diccionariomedico.models.User;
import com.github.clans.fab.FloatingActionButton;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beltran.yhoseph.diccionariomedico.models.Area;
import com.beltran.yhoseph.diccionariomedico.models.Concept;
import com.dataaccess.yhoseph.ORM.database.Operators;
import com.dataaccess.yhoseph.ORM.database.Queries;
import com.dataaccess.yhoseph.ORM.models.DBEntity;
import com.github.clans.fab.FloatingActionMenu;

import java.util.List;

public class Details extends AppCompatActivity {
    private Concept concept;
    private Menu menu;
    private FloatingActionMenu fabMenu;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        int concept_id = getIntent().getExtras().getInt("concept_id");
        Queries queries = new Queries();
        queries.AND("concept_id", Operators.EQUALS, concept_id);
        List<Concept> conceptList = DBEntity.getQuery(Concept.class, queries);
        concept = conceptList.get(0);
        TextView title = findViewById(R.id.ItemTitle);
        title.setText(concept.get_concept_name());
        TextView translate = findViewById(R.id.translate);
        translate.setText(getResources().getString(R.string.translate_label) + " " + concept.get_concept_translate());

        concept.loadData();
        Area area = concept.get_area();

        TextView areaNameItem = findViewById(R.id.areaNameItem);
        areaNameItem.setText(getResources().getString(R.string.area_label) + " " + area.get_name());
        TextView definition = findViewById(R.id.TEXT_STATUS_ID);
        definition.setMovementMethod(new ScrollingMovementMethod());
        definition.setText(concept.get_concept_definition());
        fabMenu = findViewById(R.id.fab_menu);
        updateFABItems();
    }

    private void updateFABItems() {
//        if (!concept.isConcept_custom()) {
//            FloatingActionButton fabItem = findViewById(R.id.fab_edit);
//            fabItem.hide(true);
//            fabItem.setVisibility(View.GONE);
//        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.concept_menu, menu);
        this.menu = menu;
        updateMenu();
        return true;
    }

    private void updateMenu() {
        for (int i = 0; i < menu.size(); i++) {
            if (menu.getItem(i).getItemId() == R.id.like) {
                menu.getItem(i).setVisible(concept.isConcept_like());
            }
        }
    }

    public void shareData(View view) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, createText());
        sendIntent.putExtra(Intent.EXTRA_TITLE, concept.get_concept_translate());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.app_name)));
        fabMenu.close(true);
    }

    public void likeItem(View view) {
        concept.set_concept_like(!concept.isConcept_like());
        concept.save();
        updateMenu();
        fabMenu.close(true);
    }

    public void editItem(View view) {
        if (concept.isConcept_custom()) {
            this.updateItem();
        } else {
            this.loginAdmin();
        }
    }

    private void updateItem() {
        Intent intent = new Intent(this, Edit_concept.class);
        intent.putExtra("concept_id", concept.get_concept_id());
        startActivity(intent);
    }

    private String createText() {
        StringBuilder sb = new StringBuilder();
        sb.append("*");
        sb.append(concept.get_concept_name());
        sb.append(":* ");
        sb.append(concept.get_concept_definition());
        return sb.toString();
    }

    private void loginAdmin() {
        AlertDialog.Builder mb = new AlertDialog.Builder(Details.this);
        View mv = getLayoutInflater().inflate(R.layout.login_layout, null);
        final EditText username = mv.findViewById(R.id.username);
        final EditText password = mv.findViewById(R.id.password);
        Button buttonLogin = mv.findViewById(R.id.button);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().isEmpty() || password.getText().toString().isEmpty()) {
                    Toast.makeText(Details.this, "Por favor llene todos los campos",Toast.LENGTH_SHORT).show();
                } else {
                    Queries queries = new Queries();
                    queries.AND("user_username", Operators.EQUALS, username.getText().toString());
                    queries.AND("user_password", Operators.EQUALS, password.getText().toString());
                    List<User> us = DBEntity.getQuery(User.class, queries);
                    if (us.size()>0){
                        updateItem();
                    }else{
                        Toast.makeText(Details.this, "El nombre del usuario o contraseña es incorrecta",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        mb.setView(mv);
        AlertDialog dialog = mb.create();
        dialog.show();
    }
}
