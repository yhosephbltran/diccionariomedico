package com.beltran.yhoseph.diccionariomedico;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beltran.yhoseph.diccionariomedico.models.Area;
import com.beltran.yhoseph.diccionariomedico.models.Concept;
import com.dataaccess.yhoseph.ORM.DBContext;
import com.dataaccess.yhoseph.ORM.database.Operators;
import com.dataaccess.yhoseph.ORM.database.Queries;
import com.dataaccess.yhoseph.ORM.models.DBEntity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AbsListView.OnScrollListener, View.OnClickListener {
    private TextView areaTitle;
    private List<Area> Areas;
    private int areaFilter;
    private static final int PERMISSIONS_WRITE = 1;
    private List<Concept> conceptList;
    private int preLast;
    private int startItem;
    private int limit;
    private List_adapter adapter;
    private boolean haveAccess;
    private SearchView searchView;
    private ListView listView;
    private List_adapter listAdapter;
    private NavigationView navigationView;
    private boolean isSearch;
    private LinkedHashMap<String, Integer> mapIndex;
    private String chartIndex = "*";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.areaFilter = 0;
        this.startItem = 0;
        this.limit = 50;
        this.haveAccess = getPermissions();
        this.areaTitle = findViewById(R.id.area_title);
        this.conceptList = new ArrayList<Concept>();
        this.Areas = new ArrayList<Area>();
        this.isSearch = false;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        searchViewLoad();
        getIndexList();
        displayIndex();
        listViewLoad();

        if (haveAccess) {
            start_data_base();
            fill_area();
            addMenu();
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
    private void displayIndex() {
        LinearLayout indexLayout = (LinearLayout) findViewById(R.id.side_index);

        TextView textView;
        List<String> indexList = new ArrayList<String>(mapIndex.keySet());
        for (String index : indexList) {
            textView = (TextView) getLayoutInflater().inflate(
                    R.layout.side_index_item, null);
            textView.setText(index);
            textView.setOnClickListener(this);
            indexLayout.addView(textView);
        }
    }
    public void onClick(View view) {
        this.clearData();
        TextView selectedIndex = (TextView) view;
        this.chartIndex = selectedIndex.getText().toString().toLowerCase();
        fill_concepts();
        Toast.makeText(this,selectedIndex.getText(), Toast.LENGTH_SHORT).show();
    }
    private void listViewLoad() {
        listView = findViewById(R.id.listview);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {
                openDescription(conceptList.get(position));
            }
        });
        listView.setOnScrollListener(this);
    }

    private void searchViewLoad() {
        searchView = findViewById(R.id.searchConcept);
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search_concept(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                return false;
            }

        });
    }

    private void search_concept(String query) {
        this.clearData();
        areaTitle.setText("Busqueda");
        this.isSearch = true;
        Queries queries = new Queries();
        listAdapter.setSearch(query);
        queries.AND("concept_name", Operators.LIKE, "%" + query + "%");
        queries.ORDER_BY_ASC("concept_name");
        List<Concept> lc = DBEntity.getQuery(Concept.class, queries);
        listAdapter.updateReceiptsList(lc);
        listView.setAdapter(listAdapter);
    }


    private void start_data_base() {
        DBContext.startDBContext(getApplicationContext(), "diccionario.sqlite");
        if (!DBEntity.verifyTable(Area.class)) {
            copyAssets();
        }
    }

    private void fill_area() {
        Areas = new ArrayList<Area>();
        Areas = DBEntity.getAll(Area.class);
        this.listAdapter = new List_adapter(this, conceptList, Areas);
        listView.setAdapter(listAdapter);
        fill_concepts();
    }

    private void fill_concepts() {
        startItem = 0;
        Queries queries = new Queries();
        queries.ORDER_BY_ASC("concept_name");
        if (areaFilter == 0) {
            queries.LIMIT(limit);
            if(!chartIndex.equals("*")){
                queries.AND("concept_name", Operators.LIKE, "" + chartIndex + "%");
            }
        } else {
            queries.AND("concept_id_area", Operators.EQUALS, areaFilter);
        }
        List<Concept> lc = DBEntity.getQuery(Concept.class, queries);
        listAdapter.updateReceiptsList(lc);
        listView.setAdapter(listAdapter);
    }
    private void getIndexList() {
        mapIndex = new LinkedHashMap<String, Integer>();
        mapIndex.put("*", 0);
        for (int i = 65; i <= 90; i++) {
            String item = Character.toString((char) i);
            String index = item.substring(0, 1);
            if (mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }


    private void fill_concepts(String field) {
        Queries queries = new Queries();
        queries.ORDER_BY_ASC("concept_name");
        queries.AND(field, Operators.EQUALS, true);
        List<Concept> lc = DBEntity.getQuery(Concept.class, queries);
        listAdapter.updateReceiptsList(lc);
    }

    private void addMenu() {
        this.navigationView = findViewById(R.id.nav_view);
        this.navigationView.setNavigationItemSelectedListener(this);
        Menu menu = this.navigationView.getMenu();
        SubMenu Area = menu.addSubMenu("Areas");
        Area.add(0, 0, 0, "Todos");
        areaTitle.setText("Todos");
        for (int i = 0; i < Areas.size(); i++) {
            Area.add(0, Areas.get(i).get_area_id(), 0, Areas.get(i).get_name());
        }
        SubMenu others = menu.addSubMenu("Otros");
        others.add(0, -1, 0, "Favoritos");
        others.add(0, -2, 0, "Propios");
        SubMenu About = menu.addSubMenu("Informacion");
        About.add(0, -3, 0, "Acerca de...");
        MenuItem mi = menu.getItem(menu.size() - 1);
        mi.setTitle(mi.getTitle());
        menu.getItem(0).getSubMenu().getItem(0).setChecked(true);
        menu.getItem(0).getSubMenu().getItem(0).setIcon(R.drawable.ic_check_circle_black_24dp);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.new_concept_id) {
            Intent intent = new Intent(this, New_concept.class);
            startActivity(intent);
        }
        return true;
    }

    public void check_item(MenuItem item) {
        Menu menu = this.navigationView.getMenu();
        for (int i = 0, size = menu.size(); i < size; i++) {
            SubMenu SM = menu.getItem(i).getSubMenu();
            for (int j = 0; j < SM.size(); j++) {
                SM.getItem(j).setChecked(false);
                SM.getItem(j).setIcon(0);
            }
        }
        item.setChecked(true);
        item.setIcon(R.drawable.ic_check_circle_black_24dp);
        areaTitle.setText(item.getTitle().toString());
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        this.clearData();
        int id = item.getItemId();
        areaFilter = id;
        if (id == 0) {
            this.isSearch = false;
        }
        switch (id) {
            case -1:
                check_item(item);
                fill_concepts("concept_like");
                break;
            case -2:
                check_item(item);
                fill_concepts("concept_custom");
                break;
            case -3:
                openDialog();
                break;
            default:
                check_item(item);
                fill_concepts();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void openDialog() {
        final Dialog dialog = new Dialog(Home.this);
        dialog.setContentView(R.layout.dialog);
        dialog.show();
    }

    public void openDescription(Concept concept) {
        Intent intent = new Intent(this, Details.class);
        intent.putExtra("concept_id", concept.get_concept_id());
        startActivity(intent);
    }


    private void copyAssets() {
        AssetManager assetManager = getAssets();
        String[] files = null;
        try {
            files = assetManager.list("");
        } catch (IOException e) {
            Log.e("tag", "Failed to get asset file list.", e);
        }
        for (String filename : files) {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(filename);

                String outDir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";

                File outFile = new File(outDir, filename);

                out = new FileOutputStream(outFile);
                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch (IOException e) {
                Log.e("tag", "Failed to copy asset file: " + filename, e);
            }
        }
    }

    private boolean getPermissions() {
        boolean havePermissions = false;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_WRITE);
        } else {
            havePermissions = true;
        }
        return havePermissions;
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_WRITE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    start_data_base();
                    fill_area();
                    addMenu();
                } else {
                    getPermissions();
                }
            }
        }
    }
    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {}
    @Override
    public void onScroll(AbsListView lw, final int firstVisibleItem,
                         final int visibleItemCount, final int totalItemCount) {
        if (lw.getId() == R.id.listview) {
            int lastItem = firstVisibleItem + visibleItemCount;
            if (lastItem == totalItemCount) {
                if (preLast != lastItem && areaFilter == 0 && !isSearch) {
                    preLast = lastItem;
                    addNextItems();
                }
            }
        }
    }

    private void addNextItems() {
        startItem = startItem + limit;
        Queries queries = new Queries();
        if(!chartIndex.equals("*")){
            queries.AND("concept_name", Operators.LIKE, "" + chartIndex + "%");
        }
        queries.ORDER_BY_ASC("concept_name");
        queries.LIMIT_OFFSET(limit, startItem);
        List<Concept> tmpList = DBEntity.getQuery(Concept.class, queries);
        listAdapter.addReceiptsList(tmpList);
    }
    public void clearData(){
        this.chartIndex = "*";
        listAdapter.deleteSearch();
        this.isSearch = false;
    }
}
