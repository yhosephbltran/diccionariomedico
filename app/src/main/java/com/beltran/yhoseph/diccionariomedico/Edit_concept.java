package com.beltran.yhoseph.diccionariomedico;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.beltran.yhoseph.diccionariomedico.models.Area;
import com.beltran.yhoseph.diccionariomedico.models.Concept;
import com.dataaccess.yhoseph.ORM.database.Operators;
import com.dataaccess.yhoseph.ORM.database.Queries;
import com.dataaccess.yhoseph.ORM.models.DBEntity;

import java.util.List;

public class Edit_concept extends AppCompatActivity {

    private Concept concept;
    private Area area;
    private List<Area> AreaList;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.save_menu_id:
                    save_concept();
                    return true;
                case R.id.cancel_menu_id:
                    cancel_concept();
                    return true;
                case R.id.delete_menu_id:
                    delete_concept();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_concept);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.edit_menu_id);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadData();
        loadDataInView();
    }

    private void loadDataInView() {

        Spinner spinner = findViewById(R.id.area_id);
        ArrayAdapter<Area> adapter =
                new ArrayAdapter<Area>(getApplicationContext(), R.layout.spinner_item, AreaList);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(adapter);
        EditText name_edit =  findViewById(R.id.concept_name_id);
        EditText translate_edit = findViewById(R.id.concept_translate_id);
        EditText description_edit = findViewById(R.id.concept_description_id);
        name_edit.setText(concept.get_concept_name());
        translate_edit.setText(concept.get_concept_translate());
        description_edit.setText(concept.get_concept_definition());
        int position_area = 0;
        for (int i = 0; i< AreaList.size();i++){
            if(AreaList.get(i).get_area_id() == area.get_area_id()){
                position_area = i;
                break;
            }
        }
        spinner.setSelection(position_area);
    }

    private void loadData(){
        int concept_id = getIntent().getExtras().getInt("concept_id");
        Queries queries = new Queries();
        queries.AND("concept_id", Operators.EQUALS, concept_id);
        List<Concept> conceptList = DBEntity.getQuery(Concept.class, queries);
        concept = conceptList.get(0);
        concept.loadData();
        area = concept.get_area();
        AreaList = DBEntity.getAll(Area.class);
    }
    public void save_concept() {
        Spinner mySpinner = findViewById(R.id.area_id);
        Area area = AreaList.get(mySpinner.getSelectedItemPosition());
        EditText name_edit =  findViewById(R.id.concept_name_id);
        EditText translate_edit = findViewById(R.id.concept_translate_id);
        EditText description_edit = findViewById(R.id.concept_description_id);
        concept.set_concept_area_id(area.get_area_id());
        concept.set_concept_name(name_edit.getText().toString());
        concept.set_concept_translate(translate_edit.getText().toString());
        concept.set_concept_definition(description_edit.getText().toString());
        concept.save();
        Intent intent = new Intent(this, Details.class);
        intent.putExtra("concept_id", concept.get_concept_id());
        startActivity(intent);
    }
    public void cancel_concept(){
        navigateUpTo(new Intent(this, Home.class));
    }

    public void delete_concept(){
        concept.delete();
        navigateUpTo(new Intent(this, Home.class));
    }
}
