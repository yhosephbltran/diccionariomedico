package com.dataaccess.yhoseph.ORM.database;

import android.database.sqlite.SQLiteDatabase;

public interface Upgradeable {

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);
}

