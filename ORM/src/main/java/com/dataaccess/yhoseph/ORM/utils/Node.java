package com.dataaccess.yhoseph.ORM.utils;

public final class Node {
    private String Key;
    private Object Value;
    private Class typeClass;
    private boolean notNull = false;
    private boolean primary = false;
    private boolean autoIncrement = false;

    public void setKey(String key) {
        Key = key;
    }

    public void setValue(Object value) {
        Value = value;
    }

    public void setTypeClass(Class typeClass) {
        this.typeClass = typeClass;
    }

    public String getKey(){
        return this.Key;
    }

    public Object getValue(){
        return this.Value;
    }

    public Class getTypeClass(){
        return this.typeClass;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    public boolean isPrimary() {
        return primary;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public boolean isNotNull() {
        return notNull;
    }
}
