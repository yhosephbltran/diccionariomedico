package com.dataaccess.yhoseph.ORM.utils;

import com.dataaccess.yhoseph.ORM.annotations.Ignore;
import com.dataaccess.yhoseph.ORM.annotations.ManyToOne;
import com.dataaccess.yhoseph.ORM.annotations.OneToMany;
import com.dataaccess.yhoseph.ORM.annotations.Options;
import com.dataaccess.yhoseph.ORM.annotations.Table;
import com.dataaccess.yhoseph.ORM.common.Constant;
import com.dataaccess.yhoseph.ORM.common.LOG;
import com.dataaccess.yhoseph.ORM.database.Operators;
import com.dataaccess.yhoseph.ORM.database.Queries;
import com.dataaccess.yhoseph.ORM.models.DBEntity;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class Helper {

    public static List<Node> getListValues(DBEntity entity) {
        List<Node> listResult = new ArrayList<>();
        Class classEntity = entity.getClass();
        Field[] allFields = classEntity.getDeclaredFields();
        for (Field field : allFields) {
            try {
                Field f = classEntity.getDeclaredField(field.getName());
                f.setAccessible(true);
                addingField(entity, f, listResult);
            } catch (Exception e) {
                LOG.error(e);
            }
        }
        return listResult;
    }

    public static List<Pair> getFields(Class typeClass) {
        List<Pair> fieldsResult = new ArrayList<>();
        Field[] allFields = typeClass.getDeclaredFields();
        for (Field field : allFields) {
            try {
                if (Modifier.isPrivate(field.getModifiers())) {
                    if (Helper.validateField(field)) {
                        fieldsResult.add(new Pair(field.getName(), field.getType()));
                    }

                }
            } catch (Exception e) {
                LOG.error(e);
            }
        }
        return fieldsResult;
    }

    protected static void addingField(DBEntity entity, Field field, List<Node> list) {
        try {
            Object value = null;
            value = field.get(entity);
            String fieldName = field.getName();
            if (Helper.validateField(field) && fieldName != "serialVersionUID") {
                Options options = field.getAnnotation(Options.class);

                Node node = new Node();
                if (options != null) {
                    if (value != null && value.getClass().getSimpleName().equals(String.class.getSimpleName()) && options.Trim()) {
                        value = ((String) value).trim();
                        field.set(entity, value);
                    }
                    node.setNotNull(options.NotNull());
                    node.setPrimary(options.Primary());
                    node.setAutoIncrement(options.AutoIncrement());
                }

                if (value.getClass().getSimpleName().equals(Date.class.getSimpleName())) {
                    value = ((Date) value).getTime();
                }
                node.setKey(fieldName);
                node.setValue(value);
                node.setTypeClass(field.getType());
                list.add(node);
            }
        } catch (Exception e) {
            LOG.error(e);
        }

    }

    public static String getQueryCreateTable(List<Node> list, String TableName) {
        String query = "";
        try {
            StringBuilder values = new StringBuilder();
            for (Node node : list) {
                if (values.length() != 0) {
                    values.append(", ");
                }
                values.append(node.getKey());
                values.append(" ");
                values.append(getTypeValueField(node.getTypeClass().getSimpleName()));
                if (node.isPrimary()) {
                    values.append(Constant.PRIMARY);

                    if ((node.getTypeClass().getSimpleName().equals(int.class.getSimpleName()) || node.getTypeClass().getSimpleName().equals(Integer.class.getSimpleName())) && node.isAutoIncrement()) {
                        values.append(Constant.AUTO_INCREMENT);
                    }
                    values.append(Constant.NOT_NULL);
                }
            }
            query = String.format(Constant.CREATE_TABLE, TableName, values.toString());

        } catch (Exception e) {
            LOG.error(e);
        }
        return query;
    }

    public static String getQuerySelect(List<Pair> list, String tableName, Queries queries) {
        String query = "";
        try {
            StringBuilder values = new StringBuilder();
            for (Pair pair : list) {
                if (values.length() != 0) {
                    values.append(", ");
                }
                values.append(pair.getKey());
            }
            query = String.format(Constant.SELECT_TABLE, values.toString(), tableName, queries.toString());
        } catch (Exception e) {
            LOG.warning(e.getMessage());
        }
        return query;
    }

    public static String getTypeValueField(String type) {
        String typeResponse = "TEXT";
        do {
            if (type.equals(int.class.getSimpleName()) || type.equals(Integer.class.getSimpleName()) || type.equals(short.class.getSimpleName()) || type.equals(Short.class.getSimpleName())) {
                typeResponse = "INTEGER";
                break;
            }
            if (type.equals(double.class.getSimpleName()) || type.equals(float.class.getSimpleName()) || type.equals(Double.class.getSimpleName()) || type.equals(Float.class.getSimpleName())) {
                typeResponse = "REAL";
                break;
            }
            if (type.equals(long.class.getSimpleName()) || type.equals(Long.class.getSimpleName()) || type.equals(Date.class.getSimpleName()) || type.equals(boolean.class.getSimpleName()) || type.equals(Boolean.class.getSimpleName())) {
                typeResponse = "NUMERIC";
                break;
            }
        } while (false);
        return typeResponse;
    }

    public static String getTableName(Class typeClass) {
        Table table = (Table) typeClass.getAnnotation(Table.class);
        String TableName = "";
        if (table != null && !table.name().equals("")) {
            TableName = table.name();
        } else {
            TableName = typeClass.getSimpleName();
        }
        return TableName;
    }

    public static Object getInstance(Class baseClass, Object[] paramsConstructor, Class[] paramsConstructorTypes) {
        Object object = null;
        try {
            Constructor[] listConstructors = baseClass.getConstructors();
            for (Constructor listConstructor : listConstructors) {
                if (listConstructor.getParameterTypes().length == 0) {
                    object = listConstructor.newInstance(paramsConstructor);
                    break;
                }
            }
        } catch (Exception e) {
            LOG.error(e);
        }
        return object;
    }

    public static boolean isNullOrEmpty(String text) {
        return (text == null || text.trim().equals("null") || text.trim().length() <= 0);
    }

    public static void updateValues(DBEntity dbEntity, DBEntity tmp, List<Node> listValues) {
        if (tmp != null) {
            for (Node node : listValues) {
                try {
                    Field f = tmp.getClass().getDeclaredField(node.getKey());
                    f.setAccessible(true);
                    Field f2 = dbEntity.getClass().getDeclaredField(node.getKey());
                    f2.setAccessible(true);
                    f2.set(dbEntity, f.get(tmp));
                } catch (NoSuchFieldException e) {
                    LOG.error(e);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static boolean isNewItem(DBEntity entity) {
        boolean isNew = true;
        Class classEntity = entity.getClass();
        Field[] allFields = classEntity.getDeclaredFields();
        for (Field field : allFields) {
            try {
                Ignore ignore = field.getAnnotation(Ignore.class);
                if (ignore == null) {
                    field.setAccessible(true);
                    Object value = field.get(entity);
                    Options options = field.getAnnotation(Options.class);
                    if (options != null && options.Primary()) {
                        if (isNumber(value.getClass().getSimpleName())) {
                            if (!value.toString().equals("0")) {
                                isNew = false;
                            }
                        } else if (!value.toString().equals("")) {
                            isNew = false;
                        }
                    }
                }
            } catch (Exception e) {
                LOG.error(e);
            }
        }
        return isNew;
    }

    public static List<Field> getFieldsToLoad(DBEntity dbEntity) {
        List<Field> fieldsResult = new ArrayList<>();
        Class classEntity = dbEntity.getClass();
        Field[] allFields = classEntity.getDeclaredFields();
        for (Field field : allFields) {
            try {

                Field f = classEntity.getDeclaredField(field.getName());
                f.setAccessible(true);
                if (f.isAnnotationPresent(OneToMany.class) || f.isAnnotationPresent(ManyToOne.class)) {
                    fieldsResult.add(f);
                }
            } catch (Exception e) {
                LOG.error(e);
            }
        }
        return fieldsResult;
    }

    public static void loadFields(List<Field> fields, DBEntity dbEntity) {
        for (Field f : fields) {
            try {
                ManyToOne manyToOne = f.getAnnotation(ManyToOne.class);
                OneToMany oneToMany = f.getAnnotation(OneToMany.class);
                Queries queries = new Queries();
                Field field = dbEntity.getClass().getDeclaredField(manyToOne.manyId() != null ? manyToOne.manyId() : oneToMany.oneId());
                field.setAccessible(true);
                Object value = field.get(dbEntity);
                if (!isNumber(value.getClass().getSimpleName())) {
                    value = "'" + value.toString() + "'";
                }

                if (manyToOne != null) {
                    queries.AND(manyToOne.oneId(), Operators.EQUALS, value);
                    List<Object> list = DBEntity.getQuery(f.getType(), queries);
                    if (list.size() > 0) {
                        f.set(dbEntity, list.get(0));
                    }
                } else if (oneToMany != null) {
                    queries.AND(oneToMany.manyId(), Operators.EQUALS, value);
                    List<Object> list = DBEntity.getQuery(oneToMany.many(), queries);
                    f.set(dbEntity, list);
                }
            } catch (Exception e) {
                LOG.error(e);
            }
        }
    }

    private static boolean validateField(Field field) {
        return !field.isAnnotationPresent(Ignore.class) &&
                !field.isAnnotationPresent(OneToMany.class) &&
                !field.isAnnotationPresent(ManyToOne.class);
    }


    public static boolean isNumber(String simpleName) {
        return simpleName.equals(int.class.getSimpleName()) ||
                simpleName.equals(Integer.class.getSimpleName()) ||
                simpleName.equals(Long.class.getSimpleName()) ||
                simpleName.equals(long.class.getSimpleName()) ||
                simpleName.equals(Float.class.getSimpleName()) ||
                simpleName.equals(float.class.getSimpleName()) ||
                simpleName.equals(short.class.getSimpleName()) ||
                simpleName.equals(Short.class.getSimpleName());
    }
}
