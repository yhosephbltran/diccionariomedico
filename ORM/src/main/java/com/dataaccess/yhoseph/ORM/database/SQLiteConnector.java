package com.dataaccess.yhoseph.ORM.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.dataaccess.yhoseph.ORM.common.Constant;
import com.dataaccess.yhoseph.ORM.common.LOG;
import com.dataaccess.yhoseph.ORM.utils.Helper;
import com.dataaccess.yhoseph.ORM.utils.Node;
import com.dataaccess.yhoseph.ORM.utils.Pair;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;


public class SQLiteConnector extends SQLiteOpenHelper {
    private static Upgradeable upgradeable;

    public SQLiteConnector(Context context, String DATABASE_NAME) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        upgradeable.onUpgrade(db, oldVersion, newVersion);
    }

    public void createTable(List<Node> list, String tableName) {
        try {

            String query = Helper.getQueryCreateTable(list, tableName);
            LOG.warning(query);
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(query);
        } catch (Exception e) {
            LOG.error(e);
        }
    }

    public Object insertTable(String tableName, List<Node> values) {
        Object id = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            for (Node node : values) {
                if (node.getValue() != null && !node.isAutoIncrement()) {
                    contentValues.put(node.getKey(), node.getValue().toString());
                }
            }
            id = db.insert(tableName, null, contentValues);
        } catch (Exception e) {
            LOG.error(e);
        }
        return id;
    }

    public boolean verifyTable(String tableName) {
        boolean existTable = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.close();
                    existTable = true;
                }
                cursor.close();
            }
        } catch (Exception e) {
            LOG.error(e);
        } finally {
            return existTable;
        }
    }

    public <T> List<T> runQuery(String query, Class typeClass, List<Pair> values) {
        List<T> result = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Object entity = Helper.getInstance(typeClass, new Object[]{}, new Class[]{});
                if (entity != null) {
                    for (Pair p : values) {
                        try {
                            Object value = getValue(p, cursor);
                            Field tmpField = entity.getClass().getDeclaredField(p.getKey());
                            tmpField.setAccessible(true);
                            tmpField.set(entity, value);
                        } catch (Exception e) {
                            LOG.error(e);
                        }

                    }
                    result.add((T) entity);
                }
                cursor.moveToNext();
            }
        } catch (Exception e) {
            LOG.error(e);
        }
        return result;
    }

    public <T> T getOne(String query, Class typeClass, List<Pair> values) {
        T result = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        Object entity = Helper.getInstance(typeClass, new Object[]{}, new Class[]{});
        if (entity != null) {
            for (Pair p : values) {
                try {
                    Object value = getValue(p, cursor);
                    Field tmpField = entity.getClass().getDeclaredField(p.getKey());
                    tmpField.setAccessible(true);
                    tmpField.set(entity, value);
                } catch (Exception e) {
                    LOG.error(e);
                }

            }
            result = (T) entity;
        }
        return result;
    }

    public Object getValue(Pair p, Cursor cursor) {
        Object value = null;
        do {
            if (p.getTypeClass().getSimpleName().equals(String.class.getSimpleName())) {
                value = cursor.getString(cursor.getColumnIndex(p.getKey()));
                break;
            }
            if (p.getTypeClass().getSimpleName().equals(char.class.getSimpleName()) || p.getTypeClass().getSimpleName().equals(Character.class.getSimpleName())) {
                String charText = cursor.getString(cursor.getColumnIndex(p.getKey()));
                value = Helper.isNullOrEmpty(charText) ? '\0' : charText.toCharArray()[0];
                break;
            }
            if (p.getTypeClass().getSimpleName().equals(Date.class.getSimpleName())) {
                long time = cursor.getLong(cursor.getColumnIndex(p.getKey()));
                value = new Date(time);
                break;
            }
            if (p.getTypeClass().getSimpleName().equals(short.class.getSimpleName()) || p.getTypeClass().getSimpleName().equals(Short.class.getSimpleName())) {
                value = cursor.getShort(cursor.getColumnIndex(p.getKey()));
                break;
            }
            if (p.getTypeClass().getSimpleName().equals(int.class.getSimpleName()) || p.getTypeClass().getSimpleName().equals(Integer.class.getSimpleName())) {
                value = cursor.getInt(cursor.getColumnIndex(p.getKey()));
                break;
            }
            if (p.getTypeClass().getSimpleName().equals(long.class.getSimpleName()) || p.getTypeClass().getSimpleName().equals(Long.class.getSimpleName())) {
                value = cursor.getLong(cursor.getColumnIndex(p.getKey()));
                break;
            }
            if (p.getTypeClass().getSimpleName().equals(boolean.class.getSimpleName()) || p.getTypeClass().getSimpleName().equals(Boolean.class.getSimpleName())) {
                value = cursor.getInt(cursor.getColumnIndex(p.getKey())) > 0 || cursor.getString(cursor.getColumnIndex(p.getKey())).equals("true");
                ;
                break;
            }
            if (p.getTypeClass().getSimpleName().equals(double.class.getSimpleName()) || p.getTypeClass().getSimpleName().equals(Double.class.getSimpleName())) {
                value = cursor.getDouble(cursor.getColumnIndex(p.getKey()));
                break;
            }
            if (p.getTypeClass().getSimpleName().equals(float.class.getSimpleName()) || p.getTypeClass().getSimpleName().equals(Float.class.getSimpleName())) {
                value = cursor.getFloat(cursor.getColumnIndex(p.getKey()));
                break;
            }
        } while (false);
        return value;
    }

    public void updateItemInTable(String tableName, List<Node> values) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            String selection = "";
            String[] selectionArgs = new String[1];
            for (Node node : values) {
                if (node.getValue() != null && !node.isPrimary()) {
                    contentValues.put(node.getKey(), node.getValue().toString());
                }
                if (node.isPrimary()) {
                    selection = node.getKey() + " = ?";
                    selectionArgs[0] = node.getValue().toString();
                }
            }
            db.update(tableName, contentValues, selection, selectionArgs);
        } catch (Exception e) {
            LOG.error(e);
        }
    }

    public void deleteItemInTable(String tableName, List<Node> values) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            List<String> listWhere = new ArrayList<>();
            for (Node node : values) {
                if (node.getValue() != null) {
                    if (Helper.isNumber(node.getValue().getClass().getSimpleName())) {

                        listWhere.add(node.getKey() + " = " + node.getValue().toString());
                    } else {
                        listWhere.add(node.getKey() + " = '" + node.getValue().toString() + "'");
                    }
                }
            }

            String condition = TextUtils.join(" AND ", listWhere);
            String query = String.format(Constant.DELETE_ROW, tableName, condition);
            LOG.warning(query);
            db.execSQL(query);
        } catch (Exception e) {
            LOG.error(e);
        }
    }
}
