package com.dataaccess.yhoseph.ORM.database;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Queries {
    private final static String FORMAT = "%s %s %s";
    private StringBuilder WHERE;
    private List<String> orderBy;
    private StringBuilder LIMIT;

    public Queries() {
        this.WHERE = new StringBuilder();
        this.orderBy = new ArrayList<>();
        this.LIMIT = new StringBuilder();
    }

    public void AND(String fieldName, Operator operator, Object value) {
        if (this.WHERE.length() != 0) {
            this.WHERE.append(Operators.AND.getValue());
        }
        this.WHERE.append(String.format(FORMAT, fieldName, operator.getValue(), getValue(value)));
    }

    public void OR(String fieldName, Operator operator, Object value) {
        if (this.WHERE.length() != 0) {
            this.WHERE.append(Operators.OR.getValue());
        }
        this.WHERE.append(String.format(FORMAT, fieldName, operator.getValue(), getValue(value)));
    }

    public void BETWEEN_AND(String fieldName, Date dateA, Date dateB) {
        if (this.WHERE.length() != 0) {
            this.WHERE.append(Operators.AND.getValue());
        }
        this.WHERE.append(" ( ");
        this.WHERE.append(fieldName);
        this.WHERE.append(Operators.BETWEEN.getValue());
        this.WHERE.append(getValue(dateA));
        this.WHERE.append(Operators.AND.getValue());
        this.WHERE.append(getValue(dateB));
        this.WHERE.append(" ) ");
    }

    public void BETWEEN_OR(String fieldName, Date dateA, Date dateB) {
        if (this.WHERE.length() != 0) {
            this.WHERE.append(Operators.OR.getValue());
        }
        this.WHERE.append(" ( ");
        this.WHERE.append(fieldName);
        this.WHERE.append(Operators.BETWEEN.getValue());
        this.WHERE.append(getValue(dateA));
        this.WHERE.append(Operators.AND.getValue());
        this.WHERE.append(getValue(dateB));
        this.WHERE.append(" ) ");
    }

    public void QUERY_AND(Queries queries) {
        if (queries.WHERE.length() > 0) {
            if (this.WHERE.length() != 0) {
                this.WHERE.append(Operators.AND.getValue());
            }
            this.WHERE.append(queries.getComplexQuery());
        }
    }

    public void QUERY_OR(Queries queries) {
        if (queries.WHERE.length() > 0) {
            if (this.WHERE.length() != 0) {
                this.WHERE.append(Operators.OR.getValue());
            }
            this.WHERE.append(queries.getComplexQuery());
        }
    }

    public void ORDER_BY_DEC(String fieldName) {
        this.orderBy.add(String.format(Operators.ORDER_BY_DEC.getValue(), fieldName));
    }

    public void ORDER_BY_ASC(String fieldName) {
        this.orderBy.add(String.format(Operators.ORDER_BY_ASC.getValue(), fieldName));
    }

    public void LIMIT(int limit) {
        this.LIMIT.append(Operators.LIMIT.getValue());
        this.LIMIT.append(limit);
    }

    public void LIMIT_OFFSET(int limit, int offset) {
        this.LIMIT.append(Operators.LIMIT.getValue());
        this.LIMIT.append(limit);
        this.LIMIT.append(Operators.OFFSET.getValue());
        this.LIMIT.append(offset);
    }

    private String getValue(Object value) {
        String format = "'%s'";
        String result = "";
        if (value.getClass().getSimpleName().equals(Date.class.getSimpleName())) {
            result = "" + ((Date) value).getTime();
        } else {
            result = value.toString();
        }
        return String.format(format, result);
    }

    private String getComplexQuery() {
        String format = " ( %s ) ";
        return String.format(format, this.WHERE.toString());
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (this.WHERE.length() > 0) {
            result.append(Operators.WHERE.getValue());
        }
        result.append(this.WHERE.toString());
        if (orderBy.size() > 0) {
            result.append(Operators.orderBy);
            result.append(TextUtils.join(", ", orderBy));
        }
        result.append(this.LIMIT.toString());
        return result.toString();
    }
}
