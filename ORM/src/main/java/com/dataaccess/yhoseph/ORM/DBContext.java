package com.dataaccess.yhoseph.ORM;

import android.content.Context;
import android.os.Environment;

import com.dataaccess.yhoseph.ORM.database.SQLiteConnector;

public class DBContext {
    private static DBContext dbcontext;
    private String DataBaseName;
    private Context context;
    private SQLiteConnector DB;
    private String dataBaseName;

    private DBContext(Context context) {
        this.context = context;
        this.DataBaseName = context.getPackageName();
        this.DB = new SQLiteConnector(context, this.DataBaseName);
    }

    private DBContext(Context context, String dataBaseName) {
        this.context = context;
        this.DataBaseName = dataBaseName;
        this.DB = new SQLiteConnector(context, this.DataBaseName);
    }

    public static void startDBContext(Context context) {
        if (dbcontext == null) {
            dbcontext = new DBContext(context);
        }
    }

    public static void startDBContext(Context context, String dataBaseName) {
        if (dbcontext == null) {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath();
            dbcontext = new DBContext(context, path + "/" + dataBaseName);
        }
    }

    public static DBContext getInstance() {
        return DBContext.dbcontext;
    }

    public Context getContext() {
        return context;
    }

    public String getDataBaseName() {
        return DataBaseName;
    }

    public SQLiteConnector getDB() {
        return DB;
    }
}
