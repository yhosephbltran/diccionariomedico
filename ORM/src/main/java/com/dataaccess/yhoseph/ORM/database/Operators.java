package com.dataaccess.yhoseph.ORM.database;

public class Operators {

    public final static Operator AND = new Operator(" AND ");

    public final static Operator OR = new Operator(" OR ");

    public final static Operator LIKE = new Operator(" LIKE ");

    public final static Operator EQUALS = new Operator(" = ");

    public final static Operator UNEQUALS = new Operator(" != ");

    public final static Operator WHERE = new Operator(" WHERE ");

    public final static Operator LIMIT = new Operator(" LIMIT ");

    public final static Operator OFFSET = new Operator(" OFFSET ");

    public final static Operator BETWEEN = new Operator(" BETWEEN ");

    public final static Operator ORDER_BY_DEC = new Operator(" %s DEC ");

    public final static Operator ORDER_BY_ASC = new Operator(" %s ASC ");

    public final static String orderBy = "ORDER BY";
}
