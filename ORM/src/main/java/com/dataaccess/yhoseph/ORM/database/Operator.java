package com.dataaccess.yhoseph.ORM.database;

public class Operator {
    private String value;

    protected Operator(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
