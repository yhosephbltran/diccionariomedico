package com.dataaccess.yhoseph.ORM.common;

import android.util.Log;

public class LOG {

    public static void error(Exception e) {
        Log.e(Constant.TAG, e.getMessage(), e.getCause());
    }

    public static void info(String message) {
        Log.i(Constant.TAG, message);
    }

    public static void warning(String message) {
        Log.w(Constant.TAG, message);
    }
}
