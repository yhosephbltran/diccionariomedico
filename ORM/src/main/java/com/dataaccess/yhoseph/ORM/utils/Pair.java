package com.dataaccess.yhoseph.ORM.utils;

public class Pair {
    private String key;
    private Class typeClass;

    public Pair(String key, Class typeClass){
        this.key = key;
        this.typeClass = typeClass;
    }

    public String getKey() {
        return key;
    }

    public Class getTypeClass() {
        return typeClass;
    }
}
