package com.dataaccess.yhoseph.ORM.models;

import com.dataaccess.yhoseph.ORM.DBContext;
import com.dataaccess.yhoseph.ORM.common.LOG;
import com.dataaccess.yhoseph.ORM.database.Operators;
import com.dataaccess.yhoseph.ORM.database.Queries;
import com.dataaccess.yhoseph.ORM.database.SQLiteConnector;
import com.dataaccess.yhoseph.ORM.utils.Helper;
import com.dataaccess.yhoseph.ORM.utils.Node;
import com.dataaccess.yhoseph.ORM.utils.Pair;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class DBEntity {
    private String TableName;
    private DBContext dbContext = DBContext.getInstance();
    private SQLiteConnector db = dbContext.getDB();

    public DBEntity() {
        this.TableName = Helper.getTableName(this.getClass());
        if (!db.verifyTable(TableName)) {
            List<Node> listValues = Helper.getListValues(this);
            db.createTable(listValues, TableName);
        }
    }

    public void save() {
        List<Node> listValues = Helper.getListValues(this);
        Object idResult = null;
        if (Helper.isNewItem(this)) {
            idResult = db.insertTable(TableName, listValues);
        } else {
            db.updateItemInTable(TableName, listValues);
        }
        if(idResult!=null){
            updateData(listValues, (long) idResult);
        }
    }

    public void delete() {
        List<Node> listValues = Helper.getListValues(this);
        db.deleteItemInTable(TableName, listValues);
    }

    private void updateData(List<Node> listValues, long idResult) {
        DBEntity tmp = this.getByROWID(idResult);
        Helper.updateValues(this, tmp, listValues);
    }

    public static <T> List<T> getAll(Class typeClass) {
        DBContext dbContext = DBContext.getInstance();
        SQLiteConnector db = dbContext.getDB();
        List<Pair> fields = Helper.getFields(typeClass);
        String query = Helper.getQuerySelect(fields, Helper.getTableName(typeClass), new Queries());
        return db.runQuery(query, typeClass, fields);
    }

    public static <T> List<T> getQuery(Class typeClass, Queries queries) {
        DBContext dbContext = DBContext.getInstance();
        SQLiteConnector db = dbContext.getDB();
        List<Pair> fields = Helper.getFields(typeClass);
        String query = Helper.getQuerySelect(fields, Helper.getTableName(typeClass), queries);
        List<T> result = new ArrayList<>();
        try {
            result = db.runQuery(query, typeClass, fields);
        } catch (Exception e) {
            LOG.error(e);
        }
        return result;
    }

    private DBEntity getByROWID(long id) {
        DBContext dbContext = DBContext.getInstance();
        SQLiteConnector db = dbContext.getDB();
        List<Pair> fields = Helper.getFields(this.getClass());
        Queries queries = new Queries();
        queries.AND("rowid", Operators.EQUALS, id);
        String query = Helper.getQuerySelect(fields, Helper.getTableName(this.getClass()), queries);
        return db.getOne(query, this.getClass(), fields);
    }

    public String getTableName() {
        return this.TableName;
    }

    public static boolean verifyTable(Class typeClass) {
        DBContext dbContext = DBContext.getInstance();
        SQLiteConnector db = dbContext.getDB();
        String TableName = Helper.getTableName(typeClass);
        return db.verifyTable(TableName);
    }

    public void loadData(){
        List<Field> fields = Helper.getFieldsToLoad(this);
        if(fields.size()>0){
            Helper.loadFields(fields, this);
        }
    }
}
