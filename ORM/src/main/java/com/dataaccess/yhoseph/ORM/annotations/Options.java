package com.dataaccess.yhoseph.ORM.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Options {

    boolean Trim() default false;

    boolean NotNull() default false;

    boolean Primary() default false;

    boolean AutoIncrement() default false;
}
