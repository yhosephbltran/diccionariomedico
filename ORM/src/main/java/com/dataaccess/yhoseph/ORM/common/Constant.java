package com.dataaccess.yhoseph.ORM.common;

public final class Constant {

    public final static String TAG = "DBContext";

    public final static String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS '%s'(%s);";

    public final static String DELETE_ROW = "DELETE FROM %s WHERE %s;";

    public final static String SELECT_TABLE = "SELECT %s FROM '%s' %s;";

    public final static String OR = " OR ";

    public final static String AND = " AND ";

    public final static String WHERE = "WHERE ";

    public final static String NOT_NULL = " NOT NULL ";

    public final static String AUTO_INCREMENT = " AUTOINCREMENT ";

    public final static String PRIMARY = " PRIMARY KEY ";
}
